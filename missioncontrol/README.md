# Environment
* Maven
* JDK17

# Execution
`mvn clean install`

`mvn -q spring-boot:run -Dspring-boot.run.arguments="-i <input-file_path>"`

## Example
`mvn -q spring-boot:run -Dspring-boot.run.arguments="-i src/test/resources/test-telemetry.txt"`

# Assumptions
* input file is sorted in ascending order by timestamp