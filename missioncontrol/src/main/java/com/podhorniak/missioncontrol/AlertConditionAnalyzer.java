package com.podhorniak.missioncontrol;

import com.podhorniak.missioncontrol.conditions.AlertCondition;
import com.podhorniak.missioncontrol.conditions.BatteryRedLowAlertCondition;
import com.podhorniak.missioncontrol.conditions.ThermostatRedHighAlertCondition;
import com.podhorniak.missioncontrol.models.AlertMessage;
import com.podhorniak.missioncontrol.models.TelemetryData;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Alert condition analyzer.
 */
public class AlertConditionAnalyzer {

    /**
     * The Alert conditions by satellite.
     */
    private final Map<Integer, List<AlertCondition>> alertConditionsBySatellite = new HashMap<>();

    /**
     * Analyze list.
     *
     * @param inputFileName the input file name
     * @return the list
     * @throws Exception the exception
     */
    public List<AlertMessage> analyze(String inputFileName) throws Exception {
        try(TelemetryParser parser = new TelemetryParser(inputFileName)) {
            List<AlertMessage> alertMessages = new ArrayList<>();

            while (parser.hasNext()) {
                TelemetryData telemetryData = parser.readLine();
                alertMessages.addAll(analyzeData(telemetryData));
            }

            return alertMessages;
        }
    }

    /**
     * Analyze data list.
     *
     * @param telemetryData the telemetry data
     * @return the list
     */
    private List<AlertMessage> analyzeData(TelemetryData telemetryData) {
        Integer satelliteId = telemetryData.getSatelliteId();
        if (!alertConditionsBySatellite.containsKey(satelliteId)) {
            initConditionsForSatellite(satelliteId);
        }

        return alertConditionsBySatellite.get(satelliteId).stream()
                .map(ac -> ac.analyze(telemetryData))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    /**
     * Init conditions for satellite.
     *
     * @param satelliteId the satellite id
     */
    private void initConditionsForSatellite(Integer satelliteId) {
        alertConditionsBySatellite.put(satelliteId, Arrays.asList(
                new BatteryRedLowAlertCondition(),
                new ThermostatRedHighAlertCondition()
        ));
    }
}
