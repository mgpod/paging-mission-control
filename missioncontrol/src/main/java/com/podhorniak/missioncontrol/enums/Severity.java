package com.podhorniak.missioncontrol.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * The enum Severity.
 */
@Getter
@RequiredArgsConstructor
public enum Severity {
    /**
     * The Red low.
     */
    RED_LOW("RED LOW"),
    /**
     * The Red high.
     */
    RED_HIGH("RED HIGH");

    /**
     * The Display value.
     */
    private final String displayValue;
}
