package com.podhorniak.missioncontrol.enums;

/**
 * The enum Component.
 */
public enum Component {
    /**
     * Thermostart component.
     */
    TSTAT,

    /**
     * Battery component.
     */
    BATT;
}
