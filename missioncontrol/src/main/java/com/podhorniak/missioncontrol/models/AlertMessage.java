package com.podhorniak.missioncontrol.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.podhorniak.missioncontrol.enums.Component;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * The type Alert message.
 */
@Data
@Builder
public class AlertMessage {

    /**
     * The Satellite id.
     */
    private int satelliteId;

    /**
     * The Severity.
     */
    private String severity;

    /**
     * The Component.
     */
    private Component component;

    /**
     * The Timestamp.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime timestamp;
}
