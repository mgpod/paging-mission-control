package com.podhorniak.missioncontrol.models;

import com.podhorniak.missioncontrol.enums.Component;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Telemetry data POJO.
 */
@Data
@Builder
public class TelemetryData {

    /**
     * The Timestamp.
     */
    private LocalDateTime timestamp;

    /**
     * The Satellite id.
     */
    private Integer satelliteId;

    /**
     * The Read high limit.
     */
    private int readHighLimit;

    /**
     * The Yellow high limit.
     */
    private int yellowHighLimit;

    /**
     * The Yellow low limit.
     */
    private int yellowLowLimit;

    /**
     * The Red low limit.
     */
    private int redLowLimit;

    /**
     * The Raw value.
     */
    private double rawValue;

    /**
     * The Component.
     */
    private Component component;
}
