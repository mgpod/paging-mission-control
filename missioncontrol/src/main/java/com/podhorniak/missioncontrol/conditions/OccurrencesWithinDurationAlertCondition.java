package com.podhorniak.missioncontrol.conditions;

import com.podhorniak.missioncontrol.models.AlertMessage;
import com.podhorniak.missioncontrol.models.TelemetryData;

import java.time.Duration;
import java.util.ArrayDeque;
import java.util.Optional;

/**
 * Alert condition for occurrences within a duration window.
 */
public abstract class OccurrencesWithinDurationAlertCondition implements AlertCondition {

    /**
     * The number of occurrences.
     */
    private final int occurrences;

    /**
     * The duration window.
     */
    private final Duration duration;

    /**
     * The recent alertable telemetry data.
     */
    private final ArrayDeque<TelemetryData> recentAlertableData;

    /**
     * Instantiates a new Occurrences within duration alert condition.
     *
     * @param occurrences the occurrences
     * @param duration    the duration
     */
    protected OccurrencesWithinDurationAlertCondition(int occurrences, Duration duration) {
        this.occurrences = occurrences;
        this.duration = duration;
        this.recentAlertableData = new ArrayDeque<>(occurrences);
    }

    /**
     * Determines if the data meets the alert condition.
     *
     * @param telemetryData the telemetry data
     * @return the boolean
     */
    protected abstract boolean isAlertable(TelemetryData telemetryData);

    /**
     * Gets severity.
     *
     * @return the severity
     */
    protected abstract String getSeverity();

    @Override
    public Optional<AlertMessage> analyze(TelemetryData telemetryData) {
        AlertMessage alertMessage = null;

        if (isAlertable(telemetryData)) {
            recentAlertableData.addLast(telemetryData);

            if (recentAlertableData.size() == occurrences) {
                if (isWithinDurationWindow()) {
                    alertMessage = AlertMessage.builder()
                            .satelliteId(telemetryData.getSatelliteId())
                            .component(recentAlertableData.getFirst().getComponent())
                            .severity(getSeverity())
                            .timestamp(recentAlertableData.getFirst().getTimestamp())
                            .build();
                }

                recentAlertableData.removeFirst();
            }
        }

        return Optional.ofNullable(alertMessage);
    }

    /**
     * Determines if the alertable data is winth the duration window.
     *
     * @return the boolean
     */
    private boolean isWithinDurationWindow() {
        Duration dataDuration = Duration.between(
                recentAlertableData.getFirst().getTimestamp(), recentAlertableData.getLast().getTimestamp());
        return dataDuration.compareTo(duration) <= 0;
    }

}
