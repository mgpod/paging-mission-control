package com.podhorniak.missioncontrol.conditions;

import com.podhorniak.missioncontrol.enums.Component;
import com.podhorniak.missioncontrol.enums.Severity;
import com.podhorniak.missioncontrol.models.TelemetryData;

import java.time.Duration;

/**
 * The Thermostat red high alert condition.
 */
public class ThermostatRedHighAlertCondition extends OccurrencesWithinDurationAlertCondition {

    /**
     * Instantiates a new Thermostat red high alert condition.
     */
    public ThermostatRedHighAlertCondition() {
        super(3, Duration.parse("PT5M"));
    }

    @Override
    protected boolean isAlertable(TelemetryData telemetryData) {
        return Component.TSTAT.equals(telemetryData.getComponent()) &&
                telemetryData.getReadHighLimit() < telemetryData.getRawValue();
    }

    @Override
    protected String getSeverity() {
        return Severity.RED_HIGH.getDisplayValue();
    }

}
