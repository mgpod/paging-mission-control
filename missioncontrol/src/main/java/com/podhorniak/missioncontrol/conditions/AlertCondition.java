package com.podhorniak.missioncontrol.conditions;

import com.podhorniak.missioncontrol.models.AlertMessage;
import com.podhorniak.missioncontrol.models.TelemetryData;

import java.util.Optional;

/**
 * The interface Alert condition.
 */
public interface AlertCondition {

    /**
     * Analyze optional.
     *
     * @param telemetryData the telemetry data
     * @return the optional alert message
     */
    Optional<AlertMessage> analyze(TelemetryData telemetryData);
}
