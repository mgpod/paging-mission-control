package com.podhorniak.missioncontrol.conditions;

import com.podhorniak.missioncontrol.enums.Component;
import com.podhorniak.missioncontrol.enums.Severity;
import com.podhorniak.missioncontrol.models.TelemetryData;

import java.time.Duration;

/**
 * The Battery red low alert condition.
 */
public class BatteryRedLowAlertCondition extends OccurrencesWithinDurationAlertCondition {

    /**
     * Instantiates a new Battery red low alert condition.
     */
    public BatteryRedLowAlertCondition() {
        super(3, Duration.parse("PT5M"));
    }

    @Override
    protected boolean isAlertable(TelemetryData telemetryData) {
        return Component.BATT.equals(telemetryData.getComponent()) &&
                telemetryData.getRedLowLimit() > telemetryData.getRawValue();
    }

    @Override
    protected String getSeverity() {
        return Severity.RED_LOW.getDisplayValue();
    }
}
