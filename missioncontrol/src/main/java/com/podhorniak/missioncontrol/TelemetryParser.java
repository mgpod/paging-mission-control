package com.podhorniak.missioncontrol;

import com.podhorniak.missioncontrol.enums.Component;
import com.podhorniak.missioncontrol.models.TelemetryData;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.StringTokenizer;

/**
 * Parses telemetry log files.
 */
public class TelemetryParser implements Closeable {

    /**
     * The constant TIMESTAMP_FORMAT.
     */
    private static final String TIMESTAMP_FORMAT = "yyyyMMdd HH:mm:ss.SSS";

    /**
     * The constant TIMESTAMP_FORMATTER.
     */
    private static final DateTimeFormatter TIMESTAMP_FORMATTER = DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT);

    /**
     * The File reader.
     */
    private FileReader fileReader = null;
    /**
     * The Buffered reader.
     */
    private BufferedReader bufferedReader = null;

    /**
     * Instantiates a new Telemetry parser.
     *
     * @param inputFilePath the input file path
     * @throws FileNotFoundException the file not found exception
     */
    public TelemetryParser(String inputFilePath) throws FileNotFoundException {
        if (!Files.exists(Path.of(inputFilePath))) {
            throw new IllegalArgumentException("Telemetry data input file does not exist!");
        }

        fileReader = new FileReader(inputFilePath);
        bufferedReader = new BufferedReader(fileReader);
    }

    /**
     * Has next boolean.
     *
     * @return the boolean
     * @throws IOException the io exception
     */
    public boolean hasNext() throws IOException {
        if (bufferedReader != null) {
            return bufferedReader.ready();
        }
        return false;
    }

    /**
     * Read line of telemetry data.
     *
     * @return the telemetry data
     * @throws IOException on error reading file
     */
    public TelemetryData readLine() throws IOException {
        StringTokenizer tokenizer = new StringTokenizer(bufferedReader.readLine(), "|");
        return TelemetryData.builder()
                .timestamp(LocalDateTime.parse(tokenizer.nextToken(), TIMESTAMP_FORMATTER))
                .satelliteId(Integer.valueOf(tokenizer.nextToken()))
                .readHighLimit(Integer.valueOf(tokenizer.nextToken()))
                .yellowHighLimit(Integer.valueOf(tokenizer.nextToken()))
                .yellowLowLimit(Integer.valueOf(tokenizer.nextToken()))
                .redLowLimit(Integer.valueOf(tokenizer.nextToken()))
                .rawValue(Double.valueOf(tokenizer.nextToken()))
                .component(Component.valueOf(tokenizer.nextToken()))
                .build();
    }

    /**
     * Close the telemetry data file.
     * @throws IOException on error closing file
     */
    @Override
    public void close() throws IOException {
        if (fileReader != null) {
            fileReader.close();
        }

        if (bufferedReader != null) {
            bufferedReader.close();
        }
    }
}
