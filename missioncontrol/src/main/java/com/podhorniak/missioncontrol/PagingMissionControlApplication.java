package com.podhorniak.missioncontrol;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.podhorniak.missioncontrol.models.AlertMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

/**
 * The main class.
 * <p>
 * {@code mvn spring-boot:run -Dspring-boot.run.arguments="-i src/test/resources/test-telemetry.txt"}
 */
@Slf4j
@SpringBootApplication
public class PagingMissionControlApplication implements ApplicationRunner {

	/**
	 * Main method.
	 *
	 * @param args program arguments
	 */
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(PagingMissionControlApplication.class);
		application.setLogStartupInfo(false);
		application.setBannerMode(Banner.Mode.OFF);
		application.setAddCommandLineProperties(false);
		application.run(args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		String inputFile = parseArguments(args);

		AlertConditionAnalyzer alertConditionAnalyzer = new AlertConditionAnalyzer();
		List<AlertMessage> alertMessages = alertConditionAnalyzer.analyze(inputFile);

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(alertMessages));
	}

	/**
	 * Parse application arguments.
	 *
	 * @param args the args
	 * @return the telemetry input file path
	 * @throws ParseException the parse exception
	 */
	private String parseArguments(ApplicationArguments args) throws ParseException {
		Options options = new Options();
		options.addOption("i", "input", true, "telemetry input file");
		options.addOption("h", "help", false,"print help");

		CommandLineParser cliParser = new DefaultParser();
		CommandLine commandLine = cliParser.parse(options, args.getSourceArgs());

		if (commandLine.hasOption('h')) {
			printHelp(options, 0);
		}

		if (!commandLine.hasOption('i')) {
			System.out.println("Missing required argument: 'input'");
			printHelp(options, 1);
		}

		return commandLine.getOptionValue("input");
	}

	/**
	 * Print help and exit.
	 *
	 * @param options  the options
	 * @param exitCode the exit code
	 */
	private void printHelp(Options options, int exitCode) {
		HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp("PagingMissionControl", options);
		System.exit(exitCode);
	}
}
