package com.podhorniak.missioncontrol;

import com.podhorniak.missioncontrol.enums.Component;
import com.podhorniak.missioncontrol.models.AlertMessage;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.expectThrows;

/**
 * Unit tests for {@link AlertConditionAnalyzer}.
 */
public class AlertConditionAnalyzerTest {

    /**
     * Tests happy path.
     *
     * @throws Exception on error
     */
    @Test
    public void testHappyPath() throws Exception {
        AlertConditionAnalyzer testSubject = new AlertConditionAnalyzer();
        List<AlertMessage> result = testSubject.analyze("src/test/resources/test-telemetry.txt");

        assertEquals(result.size(), 2);

        AlertMessage alertMessage1 = result.get(0);
        assertEquals(alertMessage1.getSatelliteId(), 1000);
        assertEquals(alertMessage1.getSeverity(), "RED HIGH");
        assertEquals(alertMessage1.getComponent(), Component.TSTAT);
        assertEquals(alertMessage1.getTimestamp(), LocalDateTime.parse("2018-01-01T23:01:38.001"));

        AlertMessage alertMessage2 = result.get(1);
        assertEquals(alertMessage2.getSatelliteId(), 1000);
        assertEquals(alertMessage2.getSeverity(), "RED LOW");
        assertEquals(alertMessage2.getComponent(), Component.BATT);
        assertEquals(alertMessage2.getTimestamp(), LocalDateTime.parse("2018-01-01T23:01:09.521"));
    }

    /**
     * Tests input file does not exist.
     *
     * @throws Exception on error
     */
    @Test
    public void testInputFileDoesNotExist() throws Exception {
        AlertConditionAnalyzer testSubject = new AlertConditionAnalyzer();
        IllegalArgumentException actualException =
                expectThrows(IllegalArgumentException.class, () -> testSubject.analyze("not-a-file"));
        assertEquals(actualException.getMessage(), "Telemetry data input file does not exist!");
    }
}
