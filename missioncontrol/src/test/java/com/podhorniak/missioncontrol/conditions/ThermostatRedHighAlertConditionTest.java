package com.podhorniak.missioncontrol.conditions;

import com.podhorniak.missioncontrol.enums.Component;
import com.podhorniak.missioncontrol.models.AlertMessage;
import com.podhorniak.missioncontrol.models.TelemetryData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.testng.Assert.assertEquals;

/**
 * Unit tests for {@link ThermostatRedHighAlertCondition}.
 */
public class ThermostatRedHighAlertConditionTest {

    /**
     * Current local date time
     */
    private LocalDateTime NOW = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

    /**
     * Tests {@link ThermostatRedHighAlertCondition#analyze(TelemetryData)}
     *
     * @param telemetryDataList the telemetry data list
     * @param expected          the expected
     */
    @Test(dataProvider = "telemetryDataProvider")
    public void testAnalyze(List<TelemetryData> telemetryDataList, AlertMessage expected) {
        ThermostatRedHighAlertCondition testSubject = new ThermostatRedHighAlertCondition();

        // get last return value
        Optional<AlertMessage> actual = telemetryDataList.stream()
                .map(testSubject::analyze)
                .reduce(Optional.empty(), (first, second) -> second);

        assertEquals(actual, Optional.ofNullable(expected));
    }

    /**
     * Telemetry Data Provider
     *
     * @return telemetry data
     */
    @DataProvider
    private Object[][] telemetryDataProvider() {
        return new Object[][]{
                // 3 occurrences - 5 minute duration
                {
                        Arrays.asList(
                                createTelemetryData(Component.TSTAT, 100.1, 0),
                                createTelemetryData(Component.TSTAT, 100.1, 150),
                                createTelemetryData(Component.TSTAT, 100.1, 300)),
                        createAlertMessage(0)
                },
                // 3 occurrences - 5 minute duration
                {
                        Arrays.asList(
                                createTelemetryData(Component.TSTAT, 100.1, 0),
                                createTelemetryData(Component.TSTAT, 100.1, 150),
                                createTelemetryData(Component.BATT, 100.1, 300),
                                createTelemetryData(Component.TSTAT, 100.1, 300)),
                        createAlertMessage(0)
                },
                // 0 occurrences - 5 minute duration
                {
                        Arrays.asList(
                                createTelemetryData(Component.TSTAT, 100, 0),
                                createTelemetryData(Component.TSTAT, 100, 150),
                                createTelemetryData(Component.TSTAT, 100, 300)),
                        null
                },
                // 3 occurrences - 5 minute 1 second duration
                {
                        Arrays.asList(
                                createTelemetryData(Component.TSTAT, 100.1, 0),
                                createTelemetryData(Component.TSTAT, 100.1, 150),
                                createTelemetryData(Component.TSTAT, 100.1, 301)),
                        null
                },
                // 2 occurrences - 5 minute duration
                {
                        Arrays.asList(
                                createTelemetryData(Component.TSTAT, 100.1, 0),
                                createTelemetryData(Component.TSTAT, 100.1, 150),
                                createTelemetryData(Component.BATT, 100.1, 300)),
                        null
                },
                // 2 occurrences - 5 minute duration
                {
                        Arrays.asList(
                                createTelemetryData(Component.TSTAT, 100.1, 0),
                                createTelemetryData(Component.TSTAT, 100.1, 150),
                                createTelemetryData(Component.TSTAT, 100, 300)),
                        null
                },
                // 2 occurrences - 5 minute duration
                {
                        Arrays.asList(
                                createTelemetryData(Component.TSTAT, 100.1, 0),
                                createTelemetryData(Component.TSTAT, 100.1, 150)),
                        null
                },
                // 1 occurrences - 5 minute duration
                {
                        Arrays.asList(
                                createTelemetryData(Component.TSTAT, 100.1, 0)),
                        null
                },
        };
    }

    /**
     * Create telemetry data telemetry data.
     *
     * @param component     the component
     * @param rawValue      the raw value
     * @param secondsOffset the seconds offset
     * @return the telemetry data
     */
    private TelemetryData createTelemetryData(Component component, double rawValue, int secondsOffset) {
        return TelemetryData.builder()
                .satelliteId(1000)
                .component(component)
                .readHighLimit(100)
                .redLowLimit(50)
                .rawValue(rawValue)
                .timestamp(NOW.plus(secondsOffset, ChronoUnit.SECONDS))
                .build();
    }

    /**
     * Create alert message alert message.
     *
     * @param secondsOffset the seconds offset
     * @return the alert message
     */
    private AlertMessage createAlertMessage(int secondsOffset) {
        return AlertMessage.builder()
                .satelliteId(1000)
                .component(Component.TSTAT)
                .severity("RED HIGH")
                .timestamp(NOW.plus(secondsOffset, ChronoUnit.SECONDS))
                .build();
    }

}
